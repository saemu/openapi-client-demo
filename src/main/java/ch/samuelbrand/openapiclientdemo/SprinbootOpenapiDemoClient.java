
package ch.samuelbrand.openapiclientdemo;

import ch.samuelbrand.ApiClient;
import ch.samuelbrand.api.DemoApi;
import ch.samuelbrand.api.modele.Greeting;
import ch.samuelbrand.api.modele.Person;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
public class SprinbootOpenapiDemoClient implements CommandLineRunner {

    private static final String LOG_PREFIX = "hello {}";

    public static void main(String[] args) {
        SpringApplication.run(SprinbootOpenapiDemoClient.class, args);
    }

    @Override
    public void run(String... args) {
        ApiClient client = new ApiClient();
        client.setBasePath("http://localhost:8080");
        DemoApi apiInstance = new DemoApi(client);
        log.info(LOG_PREFIX, hello(apiInstance));
        log.info(LOG_PREFIX, hello(apiInstance, "Samuel"));
        log.info(LOG_PREFIX, hello(apiInstance, "Samuel", "Brand", "Mr."));
    }
    private String hello(DemoApi api) {
        return api.hello();
    }

    private String hello(DemoApi api, String name) {
        return api.helloName(name);
    }

    private String hello(DemoApi api, String firstName, String lastName, String title) {
        Person person = new Person();
        person.setTitle(title);
        person.setFirstname(firstName);
        person.setLastname(lastName);
        Greeting greeting = api.helloPost(person);
        return greeting.getText();
    }
}
